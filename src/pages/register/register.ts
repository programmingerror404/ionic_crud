import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  data: any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, private http: Http, public loading: LoadingController) {
    this.data.email = '';
    this.data.response = '';
    this.http = http;
  }
  
   

Register() {
  if (this.data.name == '') {
    let alert = this.alertCtrl.create({
      title: 'Attention',
      subTitle: 'Name Field is Empty',
      buttons: ['Dismiss']
    });
    alert.present();
  }
  else
    if (this.data.email == '') {
      let alert = this.alertCtrl.create({
        title: 'Attention',
        subTitle: 'Email Field is Empty',
        buttons: ['Dismiss']
      });
      alert.present();
    }
    else if (this.data.password == '') {
      let alert1 = this.alertCtrl.create({
        title: 'Attention',
        subTitle: 'Password Field is Empty',
        buttons: ['Dismiss']
      });
      alert1.present();
    }
    else if (this.data.name != '' && this.data.email != '' && this.data.password != '') {

      let loader = this.loading.create({
        content: 'Processing please wait…',
      });
      var headers = new Headers();

      headers.append('Accept', 'application / json');

      headers.append('Content-Type', 'application/json; charset=UTF-8');

      let options = new RequestOptions({ headers: headers });
      var link = 'http://localhost/ionic_crud/src/api/register.php';
      var myData = {name:this.data.name, email: this.data.email, password: this.data.password };
      loader.present().then(() => {
        this.http.post(link, myData, options)
          .subscribe(data => {
            console.log(data);
            loader.dismiss();
            this.data.response = data['_body'];
            if (data['status'] == 200) {
              let alert = this.alertCtrl.create({
                title: 'Attention',
                subTitle: this.data.response,
                buttons: ['Dismiss']
              });
              alert.present();
            }

          }, error => {
            let alert = this.alertCtrl.create({
              title: 'Attention',
              subTitle: 'Something Went Wrong.',
              buttons: ['Dismiss']
            });
            alert.present();
          });
      });
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

}
