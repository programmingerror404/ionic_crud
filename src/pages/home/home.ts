import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  data:any = {};
  constructor(public navCtrl: NavController, public alertCtrl: AlertController, private http: Http, public loading: LoadingController) {
    this.data.email = '';
    this.data.response = '';
    this.http = http;
  }
  signUp() {
    this.navCtrl.push(RegisterPage);
  }
  signIn(){
    if(this.data.email==''){
      let alert = this.alertCtrl.create({
        title: 'Attention',
        subTitle: 'Email Field is Empty',
        buttons: ['Dismiss']
      });
      alert.present();
    } 
   else if(this.data.password==''){
      let alert1 = this.alertCtrl.create({
        title: 'Attention',
        subTitle: 'Password Field is Empty',
        buttons: ['Dismiss']
      });
      alert1.present();
    }
   else if ( this.data.email != '' &&  this.data.password != ''){

      let loader = this.loading.create({
        content: 'Processing please wait…',
      });
      var headers = new Headers();

      headers.append('Accept', 'application / json');

      headers.append('Content-Type', 'application/json; charset=UTF-8');

      let options = new RequestOptions({ headers: headers });
      var link = 'http://localhost/ionic_crud/src/api/login.php';
      var myData = { email: this.data.email, password: this.data.password };
      loader.present().then(() => {
      this.http.post(link, myData,options)
        .subscribe(data => {
          console.log(data);
          loader.dismiss();
          this.data.response = data['_body'];
          if(data['status']==200){
            let alert = this.alertCtrl.create({
              title: 'Attention',
              subTitle: this.data.response,
              buttons: ['Dismiss']
            });
            alert.present();
          }
         
        }, error => {
          let alert = this.alertCtrl.create({
            title: 'Attention',
            subTitle: 'Something Went Wrong.',
            buttons: ['Dismiss']
          });
          alert.present();
        });
      });
    }
  }
}
